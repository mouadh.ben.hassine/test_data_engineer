#import something if needed
import pandas as pd


def import_raw_data():
    shop1 = pd.read_csv("17-10-2018.3880", delimiter=";")
    shop1["Day"] = "Day1"

    shop2 = pd.read_csv("18-10-2018.3880", delimiter=";")
    shop2["Day"] = "Day2"

    back_office = pd.read_csv("back_office.csv", delimiter=",")
    back_office.pe_ref_in_enseigne = back_office.pe_ref_in_enseigne.astype(str)

    shop = pd.concat([shop1, shop2])
    shop.identifiantproduit = shop.identifiantproduit.astype(str)

    return [shop, back_office]

def process_data():
    shop = import_raw_data()[0]
    back_office = import_raw_data()[1]
    process_data = pd.merge(
        back_office,
        shop,
        how="left",
        left_on=["pe_ref_in_enseigne"],
        right_on=["identifiantproduit"]
    )

    return process_data

def average_prices():
    df = process_data().groupby("identifiantproduit")["prixproduit"].mean()
    df = pd.DataFrame(df)
    df.to_csv("average_prices.csv")

def count_products_by_categories_by_day():
    count_product=process_data().groupby(["categorieenseigne","Day"])["identifiantproduit"].count()
    count_product= pd.DataFrame(count_product)
    return count_product

def average_products_by_categories():
    average_product=process_data().groupby(["categorieenseigne","Day"])["identifiantproduit"].count()
    average_product = pd.DataFrame(average_product)
    return average_product





if __name__ == '__main__':
    raw_data_20181017 = import_raw_data()
    raw_data_20181018 = import_raw_data()

    processed_data_20181017 = process_data()
    processed_data_20181018 = process_data()
    ...
