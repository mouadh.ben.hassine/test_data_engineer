def exercise_one():
    for i in range(1, 101):
        if i % 15 == 0:
            print('Threefive')
        elif i % 5 == 0:
            print('Five')
        elif i % 3 == 0:
            print('Three')
        else:
            print(i)

def find_missing_nb(arr):
    sort_arr = arr
    sort_arr.sort()
    n = sort_arr[-1]
    total = n * (n + 1) // 2
    summation = sum(sort_arr)
    result = total - summation
    return  " --> Output: " +str(result)

def sortArray(a):
    # Store all non-negative values
    ans = []
    for i in range(len(a)):
        if (a[i] >= 0):
            ans.append(a[i])
    # Sort non-negative values
    ans = sorted(ans)
    j = 0
    for i in range(len(a)):
        if (a[i] >= 0):
            a[i] = ans[j]
            j += 1

    return "--> Output: " + str(a).strip("[]").replace(",","")





