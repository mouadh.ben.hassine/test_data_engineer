import json

# lecture


# Opening JSON file
f = open("path/products.json', encoding="utf-8")

# returns JSON object as
# a dictionary
product_list = json.load(f)
# Iterating through the json
# list
print(product_list)

# Closing file
f.close()

def clean_cat(data):
    cleaned_data = []
    for i in range(0, len(data)):
        cleaned_data.append(
            {
                "idappcat": data[i]["idappcat"].replace(",NULL", "").replace("NULL,", ""),
                "refproduitenseigne": data[i]["refproduitenseigne"],
                "libelle": data[i]["libelle"],
                "categorieenseigne": data[i]["categorieenseigne"]
            }
            )

    return cleaned_data


print(clean_cat(product_list))